﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public delegate void GameDelegate();
    public static event GameDelegate OnGameStarted;
    public static event GameDelegate OnGameOverConfirmed;
    public static GameManager Instance;
    public GameObject startPage;
    public GameObject gameOverPage;
    public GameObject countdownPage;
    public Text scoreText;
    int score = 0;
    bool gameOver = false;

    enum PageState {
        None,
        Start,
        GameOver,
        Countdown
    }

    public bool GameOver { get {return gameOver; }}

    void Awake() {
        Instance = this;
    }

    void OnEnable() {
        // subscribe to the event
        CountdownText.OnCountdownFinished += OnCountdownFinished;
        TapController.OnPlayerScored += OnPlayerScored;
        TapController.OnPlayerDied += OnPlayerDied;
    }

    void OnDisable(){
        // unsubscribe to the event
        CountdownText.OnCountdownFinished -= OnCountdownFinished;
        TapController.OnPlayerScored -= OnPlayerScored;
        TapController.OnPlayerDied -= OnPlayerDied;
    }

    void OnCountdownFinished() {
        SetPageState(PageState.None);
        OnGameStarted(); //event sent to TapController
        score = 0;
        gameOver = false;
    }

    // When event is received from Tap Controller as and when Player scored
    void OnPlayerScored() {
        score++;
        scoreText.text = score.ToString();
    }

    void OnPlayerDied() {
        gameOver = true;
        int savedScore = PlayerPrefs.GetInt("HighScore");   //????
        if (score > savedScore) { // if current score is greater than previous high score
            PlayerPrefs.SetInt("HighScore", score);
        }
 
        SetPageState(PageState.GameOver);
    }

    void SetPageState(PageState state){
        switch (state) {
            case PageState.None:
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countdownPage.SetActive(false);
                break;
            case PageState.Start:
                startPage.SetActive(true);
                gameOverPage.SetActive(false);
                countdownPage.SetActive(false);
                break;
            case PageState.GameOver:
                startPage.SetActive(false);
                gameOverPage.SetActive(true);
                countdownPage.SetActive(false);
                break;
            case PageState.Countdown:
                startPage.SetActive(false);
                gameOverPage.SetActive(true);
                countdownPage.SetActive(true);
                break;
        }
    }

    public void ConfirmGameOver() {
        //activated when replay button is hit
        OnGameOverConfirmed(); //event sent to TapController
        scoreText.text = "0";
        SetPageState(PageState.Start);
    }

    public void StartGame() {
        //activated when play button is hit 
        SetPageState(PageState.Countdown);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
