﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TapController : MonoBehaviour
{
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnPlayerDied;
    public static event PlayerDelegate OnPlayerScored;
    public float tapForce = 200;
    public float tiltSmooth = 10;
    public Vector3 startPos;

    Rigidbody2D rigidbody;
    Quaternion downRotation;
    Quaternion forwardRotation;

    GameManager game;

    void Start() {
        rigidbody = GetComponent<Rigidbody2D>();
        downRotation = Quaternion.Euler(0,0,-70);
        forwardRotation = Quaternion.Euler(0,0,30);
        game = GameManager.Instance;
        rigidbody.simulated = false;
    }

    void Update() {
        if(game.GameOver) return; // no update required if game is over

        // 0 indicates left click, 1 indicates right click of the mouse
        if(Input.GetMouseButtonDown(0)){
            transform.rotation = forwardRotation;
            rigidbody.velocity = Vector3.zero;
            rigidbody.AddForce(Vector2.up * tapForce, ForceMode2D.Force);

            transform.rotation = Quaternion.Lerp(transform.rotation, downRotation, tiltSmooth * Time.deltaTime);
        }
    }

    void OnEnable() {
        GameManager.OnGameStarted += OnGameStarted;
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    void OnDisable() {
        GameManager.OnGameStarted -= OnGameStarted;
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }

    void OnGameStarted() {
        rigidbody.velocity = Vector3.zero;
        rigidbody.simulated = true; // Physics once again starts applying to player
    }

    void OnGameOverConfirmed() {
        // This setup is for before player press Play
        transform.localPosition = startPos;
        transform.rotation = Quaternion.identity; 
    }

    void OnTriggerEnter2D(Collider2D collider){
        if(collider.gameObject.tag == "ScoreZone"){
            // register a score event
            OnPlayerScored(); //event sent to GameManager
        }
        if(collider.gameObject.tag == "DeadZone"){
            rigidbody.simulated = false; // freeze the bird wherever it falls
            // register an event when player dies
            OnPlayerDied(); //event sent to GameManager
        }
    }
}
